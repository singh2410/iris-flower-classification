#!/usr/bin/env python
# coding: utf-8

# # Iris Flower Classification
# #By- Aarush Kumar
# #Dated: June 18,2021

# In[2]:


get_ipython().system('pip install cufflinks')


# In[3]:


import pandas as pd
import numpy as np
import plotly
import plotly.express as px
import plotly.offline as pyo
import cufflinks as cf
from plotly.offline import init_notebook_mode,plot,iplot
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
from sklearn.metrics import accuracy_score
import os


# In[4]:


pyo.init_notebook_mode(connected=True)
cf.go_offline()


# In[5]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Iris Flower Classification/Iris.csv')


# In[6]:


df


# ## EDA

# In[7]:


df.shape


# In[8]:


df.size


# In[9]:


df.info()


# In[10]:


df.isnull().sum()


# In[11]:


df.drop('Id',axis=1,inplace=True)


# In[12]:


df


# ## Visualization

# In[13]:


px.scatter(df,x='Species',y='PetalWidthCm',size='PetalWidthCm')


# In[14]:


plt.bar(df['Species'],df['PetalWidthCm'])


# In[15]:


px.bar(df,x='Species',y='PetalWidthCm')


# In[16]:


df.iplot(kind='bar',x=['Species'],y=['PetalWidthCm'])


# In[17]:


px.line(df,x='Species',y='PetalWidthCm')


# In[18]:


df.rename(columns={'SepalLengthCm':'SepalLength','SepalWidthCm':'SepalWidth','PetalWidthCm':'PetalWidth','PetalLengthCm':'PetalLength'},inplace=True)


# In[19]:


df


# In[ ]:


px.scatter_matrix(df,color='Species',title='Iris',dimensions=['SepalLength','SepalWidth','PetalWidth','PetalLength'])


# ## Data Preprocessing

# In[25]:


df


# In[26]:


X=df.drop(['Species'],axis=1)


# In[27]:


X


# In[29]:


y=df['Species']


# In[30]:


y


# In[31]:


from sklearn.preprocessing import LabelEncoder
le=LabelEncoder()
y=le.fit_transform(y)


# In[32]:


y


# In[33]:


X


# In[34]:


X=np.array(X)


# In[35]:


X


# In[36]:


from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.3,random_state=0)


# In[37]:


X_test


# In[38]:


X_test.size


# In[39]:


df


# ## Decision Tree

# In[40]:


from sklearn import tree
DT=tree.DecisionTreeClassifier()
DT.fit(X_train,y_train)


# In[41]:


y_train.size


# In[42]:


prediction_DT=DT.predict(X_test)
acc_DT=accuracy_score(y_test,prediction_DT)*100


# In[43]:


acc_DT


# In[44]:


y_test


# In[45]:


prediction_DT


# ## Predicting on custom input value

# In[49]:


Category=['Iris-Setosa','Iris-Versicolor','Iris-Virginica']


# In[46]:


X_DT=np.array([[1 ,1, 1, 1]])
X_DT_prediction=DT.predict(X_DT)


# In[50]:


X_DT_prediction[0]
print(Category[int(X_DT_prediction[0])])


# ## KNN Algorithm

# In[51]:


from sklearn.preprocessing import StandardScaler
sc = StandardScaler().fit(X_train)  # Load the standard scaler
X_train_std=sc.transform(X_train)
X_test_std=sc.transform(X_test)


# In[52]:


from sklearn.neighbors import KNeighborsClassifier
knn=KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train_std,y_train)


# In[53]:


predict_knn=knn.predict(X_test_std)
accuracy_knn=accuracy_score(y_test,predict_knn)*100


# In[54]:


accuracy_knn


# ### prediction on custom input value

# In[55]:


X_knn=np.array([[7.7 ,3.5, 4.6, 4]])
X_knn_std=sc.transform(X_knn)
X_knn_std


# In[57]:


X_knn_prediction=knn.predict(X_knn_std)
X_knn_prediction[0]
print(Category[int(X_knn_prediction[0])])


# ### Finding Best K Value

# In[58]:


k_range=range(1,26)
scores={}
scores_list=[]
for k in k_range:
    knn=KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train_std,y_train)
    prediction_knn=knn.predict(X_test_std)
    scores[k]=accuracy_score(y_test,prediction_knn)
    scores_list.append(accuracy_score(y_test,prediction_knn))


# In[59]:


scores_list


# In[60]:


plt.plot(k_range,scores_list)


# ## K MEANS Clustering

# In[62]:


colormap=np.array(['Red','green','blue'])
fig=plt.scatter(df['PetalLength'],df['PetalWidth'],c=colormap[y],s=50)


# In[63]:


df


# In[64]:


X


# In[65]:


from sklearn.cluster import KMeans
km=KMeans(n_clusters=3,random_state=2,n_jobs=4)
km.fit(X)


# In[66]:


centers=km.cluster_centers_
print(centers)


# In[67]:


km.labels_


# In[68]:


Category_kmeans=['Iris-Versicolor', 'Iris-Setosa', 'Iris-Virginica']


# In[69]:


colormap=np.array(['Red','green','blue'])
fig=plt.scatter(df['PetalLength'],df['PetalWidth'],c=colormap[km.labels_],s=50)


# In[70]:


new_labels=km.labels_
fig,axes=plt.subplots(1,2,figsize=(16,8))
axes[0].scatter(X[:,2],X[:,3],c=y,cmap='gist_rainbow',edgecolor='k',s=150)
axes[1].scatter(X[:,2],X[:,3],c=y,cmap='jet',edgecolor='k',s=150)
axes[0].set_title('Actual',fontsize=18)
axes[1].set_title('Predicted',fontsize=18)


# ### Prediction on custom input value

# In[71]:


X_km=np.array([[1 ,1, 1, 1]])


# In[72]:


X_km_prediction=km.predict(X_km)
X_km_prediction[0]
print(Category_kmeans[int(X_km_prediction[0])])

